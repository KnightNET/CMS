KnightCMS
==================================

Egyedi fejlesztésű blog és weboldal rendszer


.. toctree::
   :maxdepth: 2
   :caption: Felhasználói dokumentáció

   install
   


.. toctree::
   :maxdepth: 1
   :caption: Fejlesztői dokumentáció
   
   role_and_permission
