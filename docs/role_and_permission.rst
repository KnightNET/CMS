.. role_and_permission

Szerepkörök és jogosultságok modul
===================
A modul lelke a Laratrust 5.0-s verziója. Dokumentációja megtalálható a következő linken: http://laratrust.readthedocs.io/en/5.0/ 

**Biztonsági beállítás:**
A beépített szerepköröket és a jogosultság jogai nem törölhetőek. Kizárólag adatbázisból! Erre azért volt szükség, hogy a funkció minden esetben működőképes maradhasson.

-------

Szerepkörök
=====

- Rendszeradminisztrátor (superadministrator)
	- Minden jog hozzáadva

----

Jogosultságok
====
- **Jogosultság modul jogai**
	- Jogosultság létrehozás (permission-create)
	- Jogosultság megtekintés (permission-show)
	- Jogosultság szerkesztés (permission-edit)
	- Jogosultság törlés (permission-delete)
	- Jogosultságok listázása (permission-list)